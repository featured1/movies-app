import MovieSchedulesIndex from "../../pages/MovieSchedules/MovieSchedulesIndex";

export default [
    {
        path: "/movies/:id/schedules",
        name: "movieSchedules",
        component: MovieSchedulesIndex,
        meta: {
            requiresAuth: true
        }
    }
];
