import SchedulesIndex from "../../pages/Schedules/SchedulesIndex";
import SchedulesCreate from "../../pages/Schedules/SchedulesCreate";

export default [
    {
        path: "/schedules",
        name: "schedules",
        component: SchedulesIndex,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/schedules/create",
        name: "createSchedule",
        component: SchedulesCreate,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/schedules/:id",
        name: "editSchedule",
        component: SchedulesCreate,
        meta: {
            requiresAuth: true
        }
    }
];
