import MoviesIndex from "../../pages/Movies/MoviesIndex";
import MoviesCreate from "../../pages/Movies/MoviesCreate";

export default [
    {
        path: "/movies",
        name: "movies",
        component: MoviesIndex,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/movies/create",
        name: "createMovie",
        component: MoviesCreate,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/movies/:id",
        name: "editMovie",
        component: MoviesCreate,
        meta: {
            requiresAuth: true
        }
    }
];
