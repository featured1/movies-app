import Vue from "vue";
import VueRouter from "vue-router";

import store from "../store";
import Login from "../pages/Login.vue";
import Dashboard from "../pages/Dashboard.vue";

import moviesRoutes from "./modules/movies";
import schedulesRoutes from "./modules/schedules";
import movieSchedulesRoutes from "./modules/movie_schedules";

Vue.use(VueRouter);

const router = new VueRouter({
    mode: "history",
    linkExactActiveClass: "active",
    routes: [
        {
            path: "/",
            name: "login",
            component: Login
        },
        {
            path: "/dashboard",
            name: "dashboard",
            component: Dashboard,
            meta: {
                requiresAuth: true
            }
        },
        ...moviesRoutes,
        ...schedulesRoutes,
        ...movieSchedulesRoutes
    ]
});

router.beforeEach(function(to, from, next) {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (store.state.auth.user.access_token === "") {
            next({
                path: "/",
                query: { redirect: to.fullPath }
            });
        } else {
            next();
        }
    } else {
        next(); // make sure to always call next()!
    }
});

export default router;
