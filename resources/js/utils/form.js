import store from "../store";
import { getNestedProperty } from "./objects";

export const generateFormData = object => {
    const formData = new FormData();
    for (const prop in object) {
        formData.append(prop, getNestedProperty(object, prop));
    }
    return formData;
};

export const setFormErrors = (vm, form) => {
    vm.$refs[form].setErrors(store.state.app.validationErrors);
    window.scrollTo({ top: 0, behavior: "smooth" });
};
