export const getNestedProperty = (object, prop) => {
    if (typeof object === "undefined") {
        return false;
    }

    const propIndex = prop.indexOf(".");
    if (propIndex > -1) {
        const nextObject = object[prop.substring(0, propIndex)];
        const nextProp = prop.substr(propIndex + 1);
        return getNestedProperty(nextObject, nextProp);
    }

    return object[prop];
};
