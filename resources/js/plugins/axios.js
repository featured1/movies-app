import axios from "axios";
import store from "../store";
import router from "../routes";

const axiosClient = axios.create();

axiosClient.interceptors.request.use(function(config) {
    config.headers.common["X-Requested-With"] = "XMLHttpRequest";

    const currentRoute = router.currentRoute.name;
    const authToken = store.state.auth.user.access_token;

    if (authToken !== "" && currentRoute !== "login") {
        config.headers.common["Authorization"] = `Bearer ${authToken}`;
    }

    return config;
});

axiosClient.interceptors.response.use(
    function(response) {
        return response;
    },
    async function(error) {
        await store.dispatch("app/setValidationErrors", []);
        if (error.response.data.errors.code === 422) {
            await store.dispatch(
                "app/setValidationErrors",
                error.response.data.errors.meta || []
            );
        }
        if (error.response.status === 401) {
            router.push({ name: "login" });
        }
        return Promise.reject(error);
    }
);

export default axiosClient;
