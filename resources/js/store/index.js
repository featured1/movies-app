import Vue from "vue";
import Vuex from "vuex";
import appModule from "./modules/app";
import authModule from "./modules/auth";
import moviesModule from "./modules/movies";
import schedulesModule from "./modules/schedules";
import createPersistedState from "vuex-persistedstate";
import movieSchedulesModule from "./modules/movie_schedules";

Vue.use(Vuex);

const persistedState = createPersistedState({
    key: "movies_app",
    paths: ["auth"]
});

const store = new Vuex.Store({
    modules: {
        app: appModule,
        auth: authModule,
        movies: moviesModule,
        schedules: schedulesModule,
        movieSchedules: movieSchedulesModule
    },

    plugins: [persistedState]
});

export default store;
