import axiosClient from "../../plugins/axios";
import { setFormErrors } from "../../utils/form";

const initialState = () => ({
    user: {
        id: "",
        name: "",
        email: "",
        access_token: ""
    }
});
const authModule = {
    namespaced: true,
    state: () => initialState(),
    actions: {
        async login({ commit }, user) {
            try {
                await axiosClient.get("/sanctum/csrf-cookie");
                const userResponse = await axiosClient.post("/api/login", user);
                commit("USER_SIGNED_IN", {
                    id: userResponse.data.data.user_id,
                    name: userResponse.data.data.attributes.name,
                    email: userResponse.data.data.attributes.email,
                    access_token: userResponse.data.data.attributes.access_token
                });

                return true;
            } catch (err) {

                this.dispatch("app/showAlert", {
                    type: "error",
                    caption: err.response.data.errors.detail || "Error on sign in try later"
                });
                return false;
            }
        },
        async logout({ commit }) {
            try {
                await axiosClient.delete("/api/logout");
                commit("USER_SIGNED_OUT");

                return true;
            } catch (err) {
                return false;
            }
        }
    },
    mutations: {
        USER_SIGNED_IN(state, user) {
            state.user = { ...user };
        },
        USER_SIGNED_OUT(state) {
            state.user = { ...initialState().user };
        }
    },
    getters: {}
};

export default authModule;
