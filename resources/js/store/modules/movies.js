import Vue from "vue";
import moment from "moment";
import axiosClient from "../../plugins/axios";
import { generateFormData } from "../../utils/form";

const moviesModule = {
    namespaced: true,
    state: () => ({
        list: [],
        total: 0
    }),
    actions: {
        async getPaginated({ commit }, options) {
            const moviesResponse = await axiosClient.get("/api/movies", {
                params: {
                    page: options.page,
                    per_page: options.itemsPerPage,
                    sort_by: options.sortBy[0] ? options.sortBy[0] : "id",
                    mode: options.sortDesc[0] ? "desc" : "asc"
                }
            });

            commit("SET_MOVIES", moviesResponse.data.data);
            commit(
                "SET_TOTAL",
                moviesResponse.data.meta
                    ? moviesResponse.data.meta.total
                    : moviesResponse.data.data.length
            );
        },
        async save(context, payload) {
            try {
                let movieId = "";
                if (payload.movieId) {
                    movieId = `/${payload.movieId}?_method=PATCH`;
                }

                const movieFormData = generateFormData(payload.data);
                await axiosClient.post("/api/movies" + movieId, movieFormData, {
                    headers: {
                        "Content-Type": "multipart/form-data"
                    }
                });

                this.dispatch("app/showAlert", {
                    type: "success",
                    caption: "The movie was stored successfully"
                });
                return true;
            } catch (err) {
                this.dispatch("app/showAlert", {
                    type: "error",
                    caption: "There was an error storing the movie"
                });
                return false;
            }
        },
        async getById(context, movieId) {
            try {
                const movie = await axiosClient.get(`/api/movies/${movieId}`);
                return movie.data.data.attributes;
            } catch (err) {
                this.dispatch("app/showAlert", {
                    type: "error",
                    caption: "There was an error getting the movie"
                });
                return false;
            }
        },
        async changeActiveStatus({ commit, state }, payload) {
            await axiosClient.patch(`/api/movies/${payload.movieId}`, {
                is_active: payload.is_active
            });

            const movie = state.list.find(
                movie => movie.id === payload.movieId
            );
            movie.is_active = payload.is_active;

            commit("UPDATE_MOVIE_ON_LIST", movie);
            this.dispatch("app/showAlert", {
                type: "success",
                caption: "The movie was updated successfully"
            });
        },
        async delete({ commit, state }, movieId) {
            await axiosClient.delete(`/api/movies/${movieId}`);
            commit("REMOVE_MOVIE", movieId);
            commit("SET_TOTAL", state.total - 1);
            this.dispatch("app/showAlert", {
                type: "success",
                caption: "The movie was deleted successfully"
            });
        }
    },
    mutations: {
        SET_MOVIES(state, movies) {
            const formattedMovies = movies.map(movie => {
                const publishDate = moment(movie.data.attributes.publish_date);
                return {
                    id: movie.data.movie_id,
                    name: movie.data.attributes.name,
                    poster: movie.data.attributes.poster,
                    publish_date: publishDate.format("DD/MMM/YYYY"),
                    is_active: movie.data.attributes.is_active
                };
            });

            state.list.splice(0);
            state.list = [...formattedMovies];
        },
        UPDATE_MOVIE_ON_LIST(state, updatedMovie) {
            const movieIndex = state.list.findIndex(
                movie => movie.id === updatedMovie.id
            );

            if (movieIndex !== -1) {
                Vue.set(state.list, movieIndex, updatedMovie);
            }
        },
        REMOVE_MOVIE(state, movieId) {
            const movieIndex = state.list.findIndex(
                movie => movie.id === movieId
            );

            if (movieIndex !== -1) {
                state.list.splice(movieIndex, 1);
            }
        },
        SET_TOTAL(state, total) {
            state.total = total;
        }
    },
    getters: {}
};

export default moviesModule;
