import Vue from "vue";
import axiosClient from "../../plugins/axios";

const schedulesModule = {
    namespaced: true,
    state: () => ({
        list: [],
        total: 0
    }),
    actions: {
        async getPaginated({ commit }, options) {
            const schedulesResponse = await axiosClient.get("/api/schedules", {
                params: {
                    page: options.page,
                    per_page: options.itemsPerPage,
                    sort_by: options.sortBy[0] ? options.sortBy[0] : "id",
                    mode: options.sortDesc[0] ? "desc" : "asc"
                }
            });

            commit("SET_SCHEDULES", schedulesResponse.data.data);
            commit(
                "SET_TOTAL",
                schedulesResponse.data.meta
                    ? schedulesResponse.data.meta.total
                    : schedulesResponse.data.data.length
            );
        },
        async save(context, payload) {
            try {
                let scheduleId = "";
                if (payload.scheduleId) {
                    scheduleId = `/${payload.scheduleId}?_method=PATCH`;
                }

                await axiosClient.post(
                    "/api/schedules" + scheduleId,
                    payload.data
                );

                this.dispatch("app/showAlert", {
                    type: "success",
                    caption: "The schedule was stored successfully"
                });
                return true;
                
            } catch (err) {
                this.dispatch("app/showAlert", {
                    type: "error",
                    caption: "There was an error storing the schedule"
                });
                return false;
            }
        },
        async getById(context, scheduleId) {
            try {
                const schedule = await axiosClient.get(
                    `/api/schedules/${scheduleId}`
                );
                return schedule.data.data.attributes;
            } catch (err) {
                this.dispatch("app/showAlert", {
                    type: "error",
                    caption: "There was an error getting the schedule"
                });
                return false;
            }
        },
        async changeActiveStatus({ commit, state }, payload) {
            await axiosClient.patch(`/api/schedules/${payload.scheduleId}`, {
                is_active: payload.is_active
            });

            const schedule = state.list.find(
                schedule => schedule.id === payload.scheduleId
            );
            schedule.is_active = payload.is_active;

            commit("UPDATE_SCHEDULE_ON_LIST", schedule);
            this.dispatch("app/showAlert", {
                type: "success",
                caption: "The schedule was updated successfully"
            });
        },
        async delete({ commit, state }, scheduleId) {
            await axiosClient.delete(`/api/schedules/${scheduleId}`);
            commit("REMOVE_SCHEDULE", scheduleId);
            commit("SET_TOTAL", state.total - 1);
            this.dispatch("app/showAlert", {
                type: "success",
                caption: "The schedule was deleted successfully"
            });
        }
    },
    mutations: {
        SET_SCHEDULES(state, schedules) {
            const formattedSchedules = schedules.map(schedule => {
                return {
                    id: schedule.data.schedule_id,
                    hour: schedule.data.attributes.hour,
                    is_active: schedule.data.attributes.is_active
                };
            });

            state.list.splice(0);
            state.list = [...formattedSchedules];
        },
        UPDATE_SCHEDULE_ON_LIST(state, updatedSchedule) {
            const scheduleIndex = state.list.findIndex(
                schedule => schedule.id === updatedSchedule.id
            );

            if (scheduleIndex !== -1) {
                Vue.set(state.list, scheduleIndex, updatedSchedule);
            }
        },
        REMOVE_SCHEDULE(state, scheduleId) {
            const scheduleIndex = state.list.findIndex(
                schedule => schedule.id === scheduleId
            );

            if (scheduleIndex !== -1) {
                state.list.splice(scheduleIndex, 1);
            }
        },
        SET_TOTAL(state, total) {
            state.total = total;
        }
    },
    getters: {}
};

export default schedulesModule;
