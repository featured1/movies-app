import moment from "moment";
import Vue from "vue";
import axiosClient from "../../plugins/axios";

const initialState = () => ({
    movie: {
        id: "",
        name: "",
        poster: "",
        publish_date: ""
    },
    dialog: {
        opened: false,
        mode: "create",
        movieScheduleId: "",
        cinema: "",
        scheduleId: ""
    },
    schedules: [],
    list: [],
    total: 0
});

const movieSchedulesModule = {
    namespaced: true,
    state: () => initialState(),
    actions: {
        async getPaginated({ commit }, options) {
            const movie = await axiosClient.get(
                `/api/movies/${options.movieId}`
            );
            commit("SET_ON_MOVIE_SCHEDULING", movie.data.data);

            const movieSchedules = await axiosClient.get(
                "/api/movie_schedules",
                {
                    params: {
                        movie_id: options.movieId,
                        page: options.page,
                        per_page: options.itemsPerPage,
                        sort_by: options.sortBy[0] ? options.sortBy[0] : "id",
                        mode: options.sortDesc[0] ? "desc" : "asc"
                    }
                }
            );
            commit("SET_MOVIE_SCHEDULES", movieSchedules.data.data);
            commit(
                "SET_TOTAL",
                movieSchedules.data.meta
                    ? movieSchedules.data.meta.total
                    : movieSchedules.data.data.length
            );
        },
        async openDialog({ commit }, payload) {
            const schedules = await axiosClient.get(`/api/schedules`, {
                params: {
                    is_active: 1,
                    sort_by: "id",
                    mode: "asc"
                }
            });
            commit("SET_SCHEDULES", schedules.data.data);
            commit("SET_DIALOG", {
                opened: true,
                ...payload
            });
        },
        closeDialog({ commit }) {
            commit("RESET_DIALOG");
        },
        async save({ commit, state }, payload) {
            try {
                let movieScheduleId = "";
                if (state.dialog.movieScheduleId) {
                    movieScheduleId = `/${state.dialog.movieScheduleId}?_method=PATCH`;
                }
                const { data } = await axiosClient.post(
                    "/api/movie_schedules" + movieScheduleId,
                    {
                        ...payload,
                        movie_id: state.movie.id
                    }
                );

                window.scroll({ top: 0, behavior: "smooth" });
                this.dispatch("app/showAlert", {
                    type: "success",
                    caption: "The movie schedule was stored successfully"
                });

                const movieSchedule = {
                    id: data.data.movie_schedule_id,
                    cinema: data.data.attributes.cinema,
                    hour: data.data.attributes.schedule.hour
                };

                // PUSH A SCHEDULE ON THE TABLE
                if (state.dialog.mode === "create") {
                    commit("ADD_MOVIE_SCHEDULE", movieSchedule);
                    commit("SET_TOTAL", state.total + 1);
                    return true;
                }

                commit("UPDATE_MOVIE_SCHEDULE", movieSchedule);

                return true;
            } catch (err) {
                this.dispatch("app/showAlert", {
                    type: "error",
                    caption: "There was an error storing the movie schedule"
                });

                return false;
            }
        },
        async getById(context, movieScheduleId) {
            try {
                const movieSchedule = await axiosClient.get(
                    `/api/movie_schedules/${movieScheduleId}`
                );
                return movieSchedule.data.data.attributes;
            } catch (err) {
                this.dispatch("app/showAlert", {
                    type: "error",
                    caption: "There was an error getting the movie schedule"
                });
                return false;
            }
        },
        async delete({ commit, state }, movieScheduleId) {
            await axiosClient.delete(`/api/movie_schedules/${movieScheduleId}`);
            commit("REMOVE_MOVIE_SCHEDULE", movieScheduleId);
            commit("SET_TOTAL", state.total - 1);
            this.dispatch("app/showAlert", {
                type: "success",
                caption: "The movie schedule was deleted successfully"
            });
        }
    },
    mutations: {
        SET_DIALOG(state, dialog) {
            state.dialog = { ...dialog };
        },
        RESET_DIALOG(state) {
            state.dialog = { ...initialState().dialog };
        },
        SET_ON_MOVIE_SCHEDULING(state, movie) {
            const publishDate = moment(movie.attributes.publish_date).format(
                "DD/MMM/YYYY"
            );

            state.movie = {
                id: movie.movie_id,
                name: movie.attributes.name,
                poster: movie.attributes.poster,
                publish_date: publishDate
            };
        },
        SET_SCHEDULES(state, schedules) {
            const realSchedules = schedules.map(schedule => ({
                id: schedule.data.schedule_id,
                hour: schedule.data.attributes.hour
            }));

            state.schedules.splice(0);
            state.schedules = [...realSchedules];
        },
        SET_MOVIE_SCHEDULES(state, movieSchedules) {
            const realMovieSchedules = movieSchedules.map(movieSchedule => {
                return {
                    id: movieSchedule.data.movie_schedule_id,
                    hour: movieSchedule.data.attributes.schedule.hour,
                    schedule_id: movieSchedule.data.attributes.schedule.id,
                    cinema: movieSchedule.data.attributes.cinema
                };
            });

            state.list.splice(0);
            state.list = [...realMovieSchedules];
        },
        ADD_MOVIE_SCHEDULE(state, movieSchedule) {
            Vue.set(state.list, state.list.length, movieSchedule);
        },
        UPDATE_MOVIE_SCHEDULE(state, newMovieSchedule) {
            const movieScheduleIndex = state.list.findIndex(
                movieSchedule => movieSchedule.id === newMovieSchedule.id
            );
            if (movieScheduleIndex !== -1) {
                Vue.set(state.list, movieScheduleIndex, newMovieSchedule);
            }
        },
        REMOVE_MOVIE_SCHEDULE(state, movieScheduleId) {
            const movieScheduleIndex = state.list.findIndex(
                movieSchedule => movieSchedule.id === movieScheduleId
            );

            if (movieScheduleIndex !== -1) {
                state.list.splice(movieScheduleIndex, 1);
            }
        },
        SET_TOTAL(state, total) {
            state.total = total;
        }
    },
    getters: {}
};

export default movieSchedulesModule;
