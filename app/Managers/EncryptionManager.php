<?php

namespace App\Managers;

use App\Interfaces\BaseEncryptionManager;
use Illuminate\Support\Facades\Hash;

class EncryptionManager implements BaseEncryptionManager
{
    public function makeHash($string)
    {
        return Hash::make($string);
    }

    public function checkHash($string, $hash)
    {
        return Hash::check($string, $hash);
    }
}
