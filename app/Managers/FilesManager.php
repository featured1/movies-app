<?php

namespace App\Managers;

use App\Exceptions\FilesException;
use App\Interfaces\BaseFilesManager;
use Illuminate\Support\Facades\Storage;

class FilesManager implements BaseFilesManager
{


    public function store($file, $path)
    {

        $fileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $sanitizedFileName = preg_replace('/[^a-z0-9]+/', '-', strtolower($fileName));

        $fullPath = $path . $sanitizedFileName . '.' . $file->getClientOriginalExtension();
        Storage::disk('local')->put('public/' . $fullPath, file_get_contents($file));

        return "storage/" . $fullPath;
    }

    public function delete($path)
    {

        $isFile = Storage::disk('local')->exists($path);

        if (!$isFile) {
            throw new FilesException("File not found", 404);
        }

        Storage::disk('local')->delete($path);
    }
}
