<?php

namespace App\Exceptions;

use Exception;

class MoviesException extends Exception
{
     /**
     * Render the exception as an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {

        if ($request->isJson()) {
            return response()->json([
                'errors' => [
                    'code'   => $this->getCode(),
                    'title'  => "Movies",
                    'detail' => $this->getMessage()
                ]
            ], $this->getCode());
        }

        abort($this->getCode(), $this->getMessage());

    }

}
