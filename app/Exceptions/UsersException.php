<?php

namespace App\Exceptions;

use Exception;

class UsersException extends Exception
{
    /**
     * Render the exception as an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {

            return response()->json([
                'errors' => [
                    'code'   => $this->getCode(),
                    'title'  => "Users",
                    'detail' => $this->getMessage()
                ]
            ], $this->getCode());

    }
}
