<?php

namespace App\Exceptions;

use Exception;

class TokenAuthException extends Exception
{
    /**
     * Render the exception as an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
  
        return response()->json([
            'errors' => [
                'code'   => 401,
                'title'  => "Authentication",
                'detail' => "Your session has been expired, please sign in again"
            ]
        ], 401);
    
    }
}
