<?php

namespace App\Exceptions;

use Exception;

class FilesException extends Exception
{
    
   /**
     * Render the exception as an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {

        if ($request->isJson()) {
            return response()->json([
                'errors' => [
                    'code'   => $this->getCode(),
                    'title'  => "Files",
                    'detail' => $this->getMessage()
                ]
            ], 404);
        }

        abort(404, 'The requested movie was not found');

    }
}
