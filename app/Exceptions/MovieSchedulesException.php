<?php

namespace App\Exceptions;

use Exception;

class MovieSchedulesException extends Exception
{
    /**
     * Render the exception as an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {

        if ($request->isJson()) {
            return response()->json([
                'errors' => [
                    'code'   => $this->getCode(),
                    'title'  => "Movie Schedules",
                    'detail' => $this->getMessage()
                ]
            ], $this->getCode());
        }

        abort($this->getCode(), $this->getMessage());
    }
}
