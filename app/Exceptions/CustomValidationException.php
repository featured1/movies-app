<?php

namespace App\Exceptions;

use Exception;

class CustomValidationException extends Exception
{
    /**
     * Render the exception as an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        return response()->json([
            'errors' => [
                'code'   => 422,
                'title'  => 'Request malformed',
                'detail' => "Your request is not valid or has missing fields",
                'meta'   => json_decode($this->getMessage())
            ]
        ],422);
    }
}
