<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MovieScheduleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            "data"  => [
                "type"       => "movie_schedules",
                "movie_schedule_id"    => $this->id,
                "attributes" => [
                    "movie"     => [
                        "id"            => $this->movie->id,
                        "name"          => $this->movie->name,
                        "publish_date"  => $this->movie->publish_date,
                        "poster"        => asset($this->movie->poster)
                    ],
                    "schedule"  => [
                        "id"    => $this->schedule->id,
                        "hour"  => $this->schedule->hour,
                    ],
                    "cinema"    => $this->cinema
                ]
            ],
            "links" => [
                "self" => url("api/movie_schedules/" . $this->id)
            ],

        ];
    }
}
