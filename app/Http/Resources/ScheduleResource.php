<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ScheduleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'data'  => [
                'type'       => 'schedules',
                'schedule_id'    => $this->id,
                'attributes' => [
                    'hour'            => $this->hour,
                    'is_active'       => $this->is_active,
                ]
            ],
            'links' => [
                'self' => url('api/schedules/' . $this->id)
            ],

        ];
    }
}
