<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LoginResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "data"  => [
                "type"       => "auth",
                "user_id"    => $this["id"],
                "attributes" => [
                    "name"            => $this["name"],
                    "email"           => $this["email"],
                    "access_token"    => $this["access_token"]
                ]
            ],
            "links" => [
                "self" => url("api/login")
            ],

        ];
    }
}
