<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MovieResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data'  => [
                'type'       => 'movies',
                'movie_id'    => $this->id,
                'attributes' => [
                    'name'            => $this->name,
                    'publish_date'    => $this->publish_date,
                    'poster'          => asset($this->poster),
                    'is_active'       => $this->is_active,
                ]
            ],
            'links' => [
                'self' => url('api/movies/' . $this->id)
            ],

        ];
    }
}
