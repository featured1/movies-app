<?php

namespace App\Http\Controllers;

use App\Managers\FilesManager;
use App\Http\Resources\MovieResource;
use App\Repositories\MoviesRepository;
use App\Http\Resources\MovieCollection;
use App\Http\Requests\Movies\IndexMovieRequest;
use App\Http\Requests\Movies\CreateMovieRequest;
use App\Http\Requests\Movies\UpdateMovieRequest;

class MovieController extends Controller
{

    private $moviesRepository;
    public function __construct(MoviesRepository $moviesRepository)
    {
        $this->moviesRepository = $moviesRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexMovieRequest $request)
    {
        $movies = $this->moviesRepository->list($request->all());
        return new MovieCollection($movies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\CreateMovieRequest
     * @param  App\Managers\FilesManager  $filesManager
     * @return \Illuminate\Http\Response
     */
    public function store(CreateMovieRequest $request, FilesManager $filesManager)
    {
        $movieData = $request->all();
        $posterPath = $filesManager->store($request->file("poster"), "movies/");
        $movieData["poster"] = $posterPath;

        $movie = $this->moviesRepository->store($movieData);
        return new MovieResource($movie);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $movie = $this->moviesRepository->findById($id);
        return new MovieResource($movie);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Managers\FilesManager  $filesManager
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMovieRequest $request, FilesManager $filesManager, $id)
    {
        $movieData = $request->all();

        if ($request->hasFile("poster")) {
            $posterPath = $filesManager->store($request->file("poster"), "movies/");
            $movieData["poster"] = $posterPath;
        }

        $movie = $this->moviesRepository->update($id, $movieData);
        return new MovieResource($movie);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->moviesRepository->delete($id);
        return response()->json(null, 204);
    }
}
