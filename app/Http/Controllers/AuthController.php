<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exceptions\UsersException;
use App\Http\Requests\LoginRequest;
use App\Managers\EncryptionManager;
use App\Http\Resources\LoginResource;
use App\Repositories\UsersRepository;

class AuthController extends Controller
{

    private $usersRepository, $encryptionManager;
    public function __construct(UsersRepository $usersRepository, EncryptionManager $encryptionManager)
    {

        $this->usersRepository = $usersRepository;
        $this->encryptionManager = $encryptionManager;
    }

    public function login(LoginRequest $request)
    {
        $user = $this->usersRepository->findByEmail($request["email"]);
        $isRightPassword = $this->encryptionManager->checkHash($request["password"],$user->password);


        if (!$isRightPassword) {
            throw new UsersException("Your credentials are incorrect", 404);
        }

        $accessToken = $user->createToken($user->name . time());

        return new LoginResource(
            array_merge(
                $user->toArray(),
                [
                    "access_token"=> $accessToken->plainTextToken
                ]
            )
        );
    }

    public function logout(Request $request){
        $request->user()->currentAccessToken()->delete();
        return response()->json(null,204);
    }
}
