<?php

namespace App\Http\Controllers;

use App\Http\Resources\ScheduleResource;
use App\Repositories\SchedulesRepository;
use App\Http\Resources\ScheduleCollection;
use App\Http\Requests\Schedules\IndexScheduleRequest;
use App\Http\Requests\Schedules\CreateScheduleRequest;
use App\Http\Requests\Schedules\UpdateScheduleRequest;

class ScheduleController extends Controller
{

    private $schedulesRepository;
    public function __construct(SchedulesRepository $schedulesRepository)
    {
        $this->schedulesRepository = $schedulesRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexScheduleRequest $request)
    {
        $schedules = $this->schedulesRepository->list($request->all());
        return new ScheduleCollection($schedules);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\CreateMovieRequest
     * @return \Illuminate\Http\Response
     */
    public function store(CreateScheduleRequest $request)
    {

        $schedule = $this->schedulesRepository->store($request->all());
        return new ScheduleResource($schedule);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $schedule = $this->schedulesRepository->findById($id);
        return new ScheduleResource($schedule);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateScheduleRequest $request, $id)
    {

        $schedule = $this->schedulesRepository->update($id, $request->all());
        return new ScheduleResource($schedule);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->schedulesRepository->delete($id);
        return response()->json(null, 204);
    }
}
