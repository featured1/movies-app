<?php

namespace App\Http\Controllers;

use App\Http\Resources\MovieScheduleResource;
use App\Repositories\MovieSchedulesRepository;
use App\Http\Resources\MovieScheduleCollection;
use App\Http\Requests\MovieSchedules\IndexMovieScheduleRequest;
use App\Http\Requests\MovieSchedules\CreateMovieScheduleRequest;
use App\Http\Requests\MovieSchedules\UpdateMovieScheduleRequest;

class MovieScheduleController extends Controller
{

    private $movieSchedulesRepository;
    public function __construct(MovieSchedulesRepository $movieSchedulesRepository)
    {
        $this->movieSchedulesRepository = $movieSchedulesRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexMovieScheduleRequest $request)
    {
        $schedules = $this->movieSchedulesRepository->list($request->all());
        return new MovieScheduleCollection($schedules);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\CreateMovieRequest
     * @return \Illuminate\Http\Response
     */
    public function store(CreateMovieScheduleRequest $request)
    {

        $schedule = $this->movieSchedulesRepository->store($request->all());
        return new MovieScheduleResource($schedule);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $schedule = $this->movieSchedulesRepository->findById($id);
        return new MovieScheduleResource($schedule);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMovieScheduleRequest $request, $id)
    {

        $schedule = $this->movieSchedulesRepository->update($id, $request->all());
        return new MovieScheduleResource($schedule);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->movieSchedulesRepository->delete($id);
        return response()->json(null, 204);
    }
}
