<?php

namespace App\Http\Requests\Movies;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMovieRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"          =>  "nullable|unique:movies,name,". $this->movie .",id,deleted_at,NULL",
            "poster"        =>  "nullable|image",
            "publish_date"  =>  "nullable|date_format:Y-m-d",
            "is_active"     =>  "nullable|in:0,1"
        ];
    }
}
