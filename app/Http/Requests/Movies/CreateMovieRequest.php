<?php

namespace App\Http\Requests\Movies;

use Illuminate\Foundation\Http\FormRequest;

class CreateMovieRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"         =>  "required|unique:movies,name,NULL,id,deleted_at,NULL",
            "poster"       =>  "required|image",
            "publish_date" =>  "required|date_format:Y-m-d"
        ];
    }
}
