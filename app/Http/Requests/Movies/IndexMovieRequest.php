<?php

namespace App\Http\Requests\Movies;

use Illuminate\Foundation\Http\FormRequest;

class IndexMovieRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "page"      => "nullable|integer|min:1",
            "per_page"  => "nullable|min:1|max:50",
            "is_active" => "nullable|in:0,1",
            "sort_by"   =>  "required|in:id,name,publish_date,is_active",
            "mode"      =>  "required|in:asc,desc"
        ];
    }
}
