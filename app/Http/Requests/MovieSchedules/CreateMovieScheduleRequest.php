<?php

namespace App\Http\Requests\MovieSchedules;

use Illuminate\Foundation\Http\FormRequest;

class CreateMovieScheduleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "movie_id" => "required|exists:App\Models\Movie,id",
            "schedule_id"   =>  "required|exists:App\Models\Schedule,id",
            "cinema"        =>  [
                "required",
                "unique:movie_schedules,cinema,NULL,id,schedule_id," . $this->request->get("schedule_id") . ",movie_id," . $this->request->get("movie_id"),
            ]
        ];
    }
}
