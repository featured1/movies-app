<?php

namespace App\Http\Requests\MovieSchedules;

use Illuminate\Foundation\Http\FormRequest;

class IndexMovieScheduleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "page"         =>  "nullable|integer|min:1",
            "per_page"     =>  "nullable|min:2|max:50",
            "movie_id"     =>  "nullable|exists:App\Models\Movie,id",
            "schedule_id"  =>  "nullable|exists:App\Models\Schedule,id",
            "sort_by"      =>  "required|in:id,movie_id,schedule_id,cinema",
            "mode"         =>  "required|in:asc,desc"
        ];
    }
}
