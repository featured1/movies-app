<?php

namespace App\Http\Requests\Schedules;

use Illuminate\Foundation\Http\FormRequest;

class UpdateScheduleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "hour" => [
                "nullable",
                "regex:/([01]?[0-9]|2[0-3]):[0-5][0-9]/",
                "unique:schedules,hour," . $this->schedule . ",id,deleted_at,NULL"
            ],
            "is_active" => "nullable|in:0,1"
        ];
    }
}
