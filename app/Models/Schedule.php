<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Schedule extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "schedules";

    protected $fillable = [
        "hour",
        "is_active"
    ];


    public function movies()
    {
        return $this->belongsToMany(Movie::class, MovieSchedule::class);
    }
}
