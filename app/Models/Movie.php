<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Movie extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "movies";

    protected $fillable = [
        "name",
        "publish_date",
        "poster",
        "is_active"
    ];

    protected $dates = ["publish_date"];

    public function schedules()
    {
        return $this->belongsToMany(Schedule::class, MovieSchedule::class);
    }
}
