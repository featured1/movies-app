<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MovieSchedule extends Pivot
{
    use HasFactory;

    protected $table = "movie_schedules";
    public $incrementing = true;


    protected $fillable = [
        "movie_id",
        "schedule_id",
        "cinema"
    ];

    public function movie()
    {
        return $this->belongsTo(Movie::class, 'movie_id', 'id');
    }

    public function schedule()
    {
        return $this->belongsTo(Schedule::class, 'schedule_id', 'id');
    }
}
