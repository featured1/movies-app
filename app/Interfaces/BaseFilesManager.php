<?php

namespace App\Interfaces;

interface BaseFilesManager
{

    public function store($file, $path);

    public function delete($path);
}
