<?php

namespace App\Interfaces;

interface BaseEncryptionManager
{
    public function makeHash($string);
    public function checkHash($string, $hash);
}
