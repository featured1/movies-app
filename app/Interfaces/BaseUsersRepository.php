<?php

namespace App\Interfaces;

interface BaseUsersRepository
{
    public function findByEmail($email);
}
