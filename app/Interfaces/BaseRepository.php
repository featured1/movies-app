<?php

namespace App\Interfaces;

interface BaseRepository
{

    public function list(array $filters);

    public function store(array $data);

    public function findById(int $id);

    public function update(int $id, array $data);

    public function delete(int $id);
}
