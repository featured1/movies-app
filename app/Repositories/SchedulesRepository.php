<?php

namespace App\Repositories;

use App\Models\Schedule;
use App\Interfaces\BaseRepository;
use App\Exceptions\SchedulesException;

class SchedulesRepository implements BaseRepository
{


    private $model;

    public function __construct()
    {
        $this->model = app()->make(Schedule::class);
    }

    public function list(array $filters)
    {
        $schedules = $this->model->query()
            ->orderBy($filters["sort_by"], $filters["mode"]);

        $schedules->when(isset($filters["is_active"]), function($query) use ($filters){
            $query->where("is_active",$filters["is_active"]);
        });

        if(isset($filters["per_page"])){
            return $schedules->paginate($filters["per_page"]);
        }
       
        return $schedules->get();
    }

    public function store(array $data)
    {
        return $this->model->create($data);
    }

    public function findById(int $id)
    {
        $schedule = $this->model->find($id);
        if (!$schedule) {
            throw new SchedulesException("Schedule not found", 404);
        }

        return $schedule;
    }

    public function update(int $id, array $data)
    {

        $schedule = $this->findById($id);
        $schedule->fill($data);
        $schedule->save();

        return $schedule;
    }

    public function delete(int $id)
    {
        $schedule = $this->findById($id);
        $schedule->movies()->detach();
        $schedule->delete();
    }
}
