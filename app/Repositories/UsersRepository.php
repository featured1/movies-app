<?php

namespace App\Repositories;

use App\Models\User;
use App\Exceptions\UsersException;
use App\Interfaces\BaseUsersRepository;

class UsersRepository implements BaseUsersRepository
{


    private $model;

    public function __construct()
    {
        $this->model = app()->make(User::class);
    }

    public function findByEmail($email)
    {

        $user = $this->model->where("email", $email)->first();
        if (!$user) {
            throw new UsersException("User not found", 404);
        }

        return $user;
    }
}
