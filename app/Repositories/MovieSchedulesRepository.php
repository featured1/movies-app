<?php

namespace App\Repositories;

use App\Models\MovieSchedule;
use App\Interfaces\BaseRepository;
use App\Exceptions\MovieSchedulesException;

class MovieSchedulesRepository implements BaseRepository
{

    private $model;

    public function __construct()
    {
        $this->model = app()->make(MovieSchedule::class);
    }


    public function list(array $filters)
    {

        $movieSchedules = $this->model->query()
            ->with(["movie", "schedule"])
            ->orderBy($filters["sort_by"], $filters["mode"]);

        $movieSchedules->when(isset($filters["movie_id"]), function ($query) use ($filters) {
            $query->where("movie_schedules.movie_id", $filters["movie_id"]);
        });

        $movieSchedules->when(isset($filters["schedule_id"]), function ($query) use ($filters) {
            $query->where("movie_schedules.schedule_id", $filters["schedule_id"]);
        });

        if (isset($filters["per_page"])) {
            return $movieSchedules->paginate($filters["per_page"]);
        }

        return $movieSchedules->get();
    }

    public function store(array $data)
    {
        return $this->model->create($data);
    }

    public function findById(int $id)
    {
        $movieSchedule = $this->model
            ->with(["movie", "schedule"])
            ->find($id);

        if (!$movieSchedule) {
            throw new MovieSchedulesException("Movie schedule not found", 404);
        }

        return $movieSchedule;
    }

    public function update(int $id, array $data)
    {

        $movieSchedule = $this->findById($id);
        $movieSchedule->fill($data);
        $movieSchedule->save();

        return $movieSchedule;
    }

    public function delete(int $id)
    {
        $movieSchedule = $this->findById($id);
        $movieSchedule->delete();
    }
}
