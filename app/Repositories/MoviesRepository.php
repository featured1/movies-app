<?php

namespace App\Repositories;

use App\Models\Movie;
use App\Interfaces\BaseRepository;
use App\Exceptions\MoviesException;

class MoviesRepository implements BaseRepository
{


    private $model;

    public function __construct()
    {
        $this->model = app()->make(Movie::class);
    }

    public function list(array $filters)
    {

        $movies = $this->model->query()
            ->orderBy($filters["sort_by"], $filters["mode"]);

        $movies->when(isset($filters["is_active"]), function ($query) use ($filters) {
            $query->where("is_active", $filters["is_active"]);
        });

        if (isset($filters["per_page"]) && $filters["per_page"] !== "-1") {
            return $movies->paginate($filters["per_page"]);
        }

        return $movies->get();
    }

    public function store(array $data)
    {
        return $this->model->create($data);
    }

    public function findById(int $id)
    {

        $movie = $this->model->find($id);
        if (!$movie) {
            throw new MoviesException("Movie not found", 404);
        }

        return $movie;
    }

    public function update(int $id, array $data)
    {

        $movie = $this->findById($id);
        $movie->fill($data);
        $movie->save();

        return $movie;
    }

    public function delete(int $id)
    {
        $movie = $this->findById($id);
        $movie->schedules()->detach();
        $movie->delete();
    }
}
