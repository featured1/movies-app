<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post("login", "App\Http\Controllers\AuthController@login")->name("login");


Route::group(["middleware" => "auth:sanctum"], function () {
    
    Route::apiResource("movies", "App\Http\Controllers\MovieController");
    Route::apiResource("schedules", "App\Http\Controllers\ScheduleController");
    Route::apiResource("movie_schedules", "App\Http\Controllers\MovieScheduleController");
    Route::delete("logout", "App\Http\Controllers\AuthController@logout")->name("logout");

});
