# Movies App

## Running the web server

### Before running the server please check your environment file:

- Database credentials

> **composer install**. Download the composer libraries

> **php artisan key:generate**. Generates your application key

> **php artisan migrate**. Creates the database tables structure

> **php artisan db:seed**. Fills the users table with 3 users by default

> **php artisan serve --port 8000**. Run the web server

## Running the SPA


> **npm install**. Downloads the neccesary node packages for the app.

> **npm run dev**. Compiles your app.



## Now you are ready to test the application
- link: http://localhost:8000
- email: trejojaimejosejesus@gmail.com
- password: password